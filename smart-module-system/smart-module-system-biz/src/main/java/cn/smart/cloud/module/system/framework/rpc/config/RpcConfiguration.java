package cn.smart.cloud.module.system.framework.rpc.config;

import cn.smart.cloud.module.infra.api.config.ConfigApi;
import cn.smart.cloud.module.infra.api.file.FileApi;
import cn.smart.cloud.module.infra.api.websocket.WebSocketSenderApi;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@EnableFeignClients(clients = {FileApi.class, WebSocketSenderApi.class, ConfigApi.class})
public class RpcConfiguration {
}
